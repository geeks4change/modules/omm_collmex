# OMM Collmex

Membership management with export to collmex.de.

## Migration omm_collmex_users

* Relies on omm_collmex_entity_presave() setting entrance and exit date.
* Relies on entrance date to export only members and not other (maybe only interested) users.
* As of matching: Matching members by mail on the fly in export did not work out smooothly,
as the api has no way to filter by mail. So later we might migrate collmex_id 
from collmex to drupal users by matching their mail.

## Migration omm_collmex_users_subscription

* Creates a subscription per user with a self assigned membership fee.

## Migration omm_collmex_product_membership

This exports a set of predefined membership products

## Deleting target collmex items

See [Migrate support for deleting items no longer in the incoming data](https://www.drupal.org/project/drupal/issues/2809433).
Collmex destination plugin has support to set such items inactive.

# TODO

* (Low prio)Ensure that membership application has membership data.
* Implement deposit and credit messages and migrations to invoices.

# Notes

* We are reluctant to implement guest membership as this might set wrong incentives. 
