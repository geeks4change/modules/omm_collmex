<?php

namespace Drupal\omm_collmex;

use Drupal\entitytools\EntityNestedProperty;
use Drupal\migrate\Event\MigrateEvents;
use Drupal\migrate\Event\MigratePostRowSaveEvent;
use Drupal\user\Entity\User;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class MigrateEventSubscriber implements EventSubscriberInterface {

  public function onPostRowSave(MigratePostRowSaveEvent $event) {
    if ($event->getMigration()->id() !== 'omm_collmex_users') {
      return;
    }
    $destIds = $event->getDestinationIdValues();
    if (!$destIds) {
      return;
    }
    $collmexId = reset($destIds);

    $sourceIds = $event->getRow()->getSourceIdValues();
    $userId = reset($sourceIds);

    $user = User::load($userId);
    $collmexIdProperty = EntityNestedProperty::create($user)
      ->getNestedObject('field_omm_membership_interested/0/entity/field_omm_collmex_id');
    if ($collmexIdProperty ) {
      /** @var \Drupal\Core\Field\FieldItemListInterface $collmexIdProperty */
      if ($collmexIdProperty->getString() != $collmexId) {
        $collmexIdProperty->setValue($collmexId);
        // Ignore host entity, just save Paragraph here.
        $collmexIdProperty->getEntity()->save();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      MigrateEvents::POST_ROW_SAVE => ['onPostRowSave']
    ];
  }

}
